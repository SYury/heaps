#include "heap.h"


LeftistHeapNode * _leftistmerge(LeftistHeapNode * l, LeftistHeapNode * r){
	if(l == nullptr)return r;
	if(r == nullptr)return l;
	if(l->key <= r->key){
		l->r = _leftistmerge(l->r, r);
		l->updrnk();
		int lr = (l->l == nullptr) ? 0 : l->l->rnk;
		int rr = (l->r == nullptr) ? 0 : l->r->rnk;
		if(lr < rr)std::swap(l->l, l->r);
		return l;
	}
	else return _leftistmerge(r, l);
}

int LeftistHeap::GetMin(){
	return root->key;
}

void LeftistHeap::Insert(int key){
	LeftistHeapNode * v = new LeftistHeapNode(key);
	_leftistmerge(root, v);
}

int LeftistHeap::ExtractMin(){
	int result = root->key;
	root = _leftistmerge(root->l, root->r);
	return result;
}

void LeftistHeap::MeldWith(IHeap * what_){
	LeftistHeap * what = reinterpret_cast<LeftistHeap*>(what_);
	root = _leftistmerge(root, what->root);
}
