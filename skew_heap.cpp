#include "heap.h"


SkewHeapNode * _skewmerge(SkewHeapNode * l, SkewHeapNode * r){
	if(l == nullptr)return r;
	if(r == nullptr)return l;
	if(l->key <= r->key){
		SkewHeapNode * tmp = l->r;
		l->r = l->l;
		l->l = _skewmerge(r, tmp);
		return l;
	}
	else return _skewmerge(r, l);
}

int SkewHeap::GetMin(){
	return root->key;
}

int SkewHeap::ExtractMin(){
	int result = root->key;
	root = _skewmerge(root->l, root->r);
	return result;
}

void SkewHeap::Insert(int key){
	SkewHeapNode * v = new SkewHeapNode(key);
	root = _skewmerge(root, v);
}

void SkewHeap::MeldWith(IHeap * what_){
	SkewHeap * what = reinterpret_cast<SkewHeap*>(what_);
	root = _skewmerge(root, what->root);
}
