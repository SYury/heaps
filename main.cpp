#include "heap.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <set>
#include <chrono>

#define die(...) fprintf(stderr, __VA_ARGS__),fflush(stderr),exit(-1)

const int N_OPERATIONS = 5e3;
bool destroyed[N_OPERATIONS];
int op_type[N_OPERATIONS];
int true_ans[N_OPERATIONS];
long long op_info[N_OPERATIONS];
std::multiset<int> stupid_heap[N_OPERATIONS];

long long pack(int l, int r){
	long long res = l;
	return (res<<31) + r;
}

void unpack(long long x, int & l, int & r){
	l = x>>31;
	r = x&((1ll<<31)-1);
}

void generate(){
	srand(time(0));
	memset(destroyed, 0, sizeof(destroyed));
	int sz = 0;
	int i = 0;
	while(i < N_OPERATIONS){
		if(i == 0){op_type[i] = 0; i++; sz++; continue;}
		int tp = rand()%5;
		if(tp == 4 && sz < 2)continue;
		if(tp > 0 && tp < 4 && sz < 1)continue;
		if(tp == 0){
			op_info[i] = rand();
			op_type[i] = 0; i++; sz++; continue;
		}
		if(tp == 1){
			int where = rand()%sz;
			for(int i = 0; i < N_OPERATIONS; i++){if(!destroyed[i]){where--;} if(where == -1){where = i; break;}}
			int key = rand();
			op_type[i] = 1; op_info[i] = pack(where, key);
			i++; continue;
		}
		if(tp == 2 || tp == 3){
			int where = rand()%sz;
			for(int i = 0; i < N_OPERATIONS; i++){if(!destroyed[i]){where--;} if(where == -1){where = i; break;}}
			op_type[i] = tp; op_info[i] = where;
			i++; continue;
		}
		if(tp == 4){
			int l = rand()%sz, r = rand()%sz;
			if(l == r)continue;
			for(int i = 0; i < N_OPERATIONS; i++){if(!destroyed[i])l--; if(l == -1){l = i; break;}}
			for(int i = 0; i < N_OPERATIONS; i++){if(!destroyed[i])r--; if(r == -1){r = i; break;}}
			destroyed[r] = true;
			op_type[i] = tp; op_info[i] = pack(l, r);
			sz--;
			i++; continue;
		}
	}
}

void StupidAddHeap(int key, int & ptr){
	stupid_heap[ptr++].insert(key);
}

void StupidInsert(int id, int key){
	stupid_heap[id].insert(key);
}

int StupidGetMin(int id){
	return *(stupid_heap[id].begin());
}

int StupidExtractMin(int id){
	int it = *(stupid_heap[id].begin());
	stupid_heap[id].erase(stupid_heap[id].begin());
	return it;
}

void StupidMeld(int l, int r){
	while(!stupid_heap[r].empty()){
		stupid_heap[l].insert(*(stupid_heap[r].begin()));
		stupid_heap[r].erase(stupid_heap[r].begin());
	}
}

void AddHeap(int key, std::vector<IHeap*> & vec, int & ptr){
	vec[ptr++]->Insert(key);
}

void Insert(int id, int key, std::vector<IHeap*> & vec){
	vec[id]->Insert(key);
}

int GetMin(int id, std::vector<IHeap*> & vec){
	return vec[id]->GetMin();
}

int ExtractMin(int id, std::vector<IHeap*> & vec){
	return vec[id]->ExtractMin();
}

void Meld(int l, int r, std::vector<IHeap*> & vec){
	vec[l]->MeldWith(vec[r]);
}

void prepareAns(){
	int ptr = 0;
	for(int i = 0; i < N_OPERATIONS; i++)stupid_heap[i].clear();
	for(int i = 0; i < N_OPERATIONS; i++){
		int tp = op_type[i];
		if(tp == 0){
			StupidAddHeap(op_info[i], ptr);
		}
		if(tp == 1){
			int l, r;
			unpack(op_info[i], l, r);
			StupidInsert(l, r);
		}
		if(tp == 2){
			int id = op_info[i];
			if(stupid_heap[id].empty())continue;
			int val = StupidGetMin(op_info[i]);
			true_ans[i] = val;
		}
		if(tp == 3){
			int id = op_info[i];
			if(stupid_heap[id].empty())continue;
			int val = StupidExtractMin(id);
			true_ans[i] = val;
		}
		if(tp == 4){
			int l, r;
			unpack(op_info[i], l, r);
			StupidMeld(l, r);
		}
	}
}

double testIHeap(std::vector<IHeap*>&  vec){
	int ptr = 0;
	std::clock_t start = std::clock();
	for(int i = 0; i < N_OPERATIONS; i++){
		int tp = op_type[i];
		if(tp == 0){
			AddHeap(op_info[i], vec, ptr);
		}
		if(tp == 1){
			int l, r;
			unpack(op_info[i], l, r);
			Insert(l, r, vec);
		}
		if(tp == 2){
			int id = op_info[i];
			if(vec[id]->empty())continue;
			int val = GetMin(op_info[i], vec);
			if(val != true_ans[i])die("Fail at GetMin: got %d expected %d from heap %d at step %d\n", val, true_ans[i], (int)op_info[i], i);
		}
		if(tp == 3){
			int id = op_info[i];
			if(vec[id]->empty())continue;
			int val = ExtractMin(id, vec);
			if(val != true_ans[i])die("Fail at ExtractMin: got %d expected %d from heap %d at step %d\n", val, true_ans[i], (int)op_info[i], i);
		}
		if(tp == 4){
			int l, r;
			unpack(op_info[i], l, r);
			Meld(l, r, vec);
		}
	}
	return (std::clock() - start)/double(CLOCKS_PER_SEC);
}

std::vector<IHeap*> tmp(N_OPERATIONS);

void testBinomialHeap(){
	std::vector<BinomialHeap*> vec(N_OPERATIONS);
	for(int i = 0; i < N_OPERATIONS; i++){vec[i] = new BinomialHeap; tmp[i] = vec[i];}
	double T = testIHeap(tmp);
	printf("Binomial Heap time elapsed: %.10lf\n", T);
}

void testLeftistHeap(){
	std::vector<LeftistHeap*> vec(N_OPERATIONS);
	for(int i = 0; i < N_OPERATIONS; i++){vec[i] = new LeftistHeap; tmp[i] = vec[i];}
	double T = testIHeap(tmp);
	printf("Leftist Heap time elapsed: %.10lf\n", T);
}

void testSkewHeap(){
	std::vector<SkewHeap*> vec(N_OPERATIONS);
	for(int i = 0; i < N_OPERATIONS; i++){vec[i] = new SkewHeap; tmp[i] = vec[i];}
	double T = testIHeap(tmp);
	printf("Skew Heap time elapsed: %.10lf\n", T);
}

void testAll(){
	generate();
	prepareAns();
	testBinomialHeap();
	testLeftistHeap();
	testSkewHeap();
}

int main(){
	testAll();
	return 0;
}

