#include<algorithm>
#include<limits.h>
#include<stdio.h>

class IHeap{
	public:
		virtual ~IHeap(){};
		virtual bool empty() = 0;
		virtual int GetMin() = 0;
		virtual void Insert(int key) = 0;
		virtual int ExtractMin() = 0;
		virtual void MeldWith(IHeap * what) = 0;
};
struct BinomialHeapNode{
	int key;
	std::vector<BinomialHeapNode*> children;
	BinomialHeapNode(){}
	BinomialHeapNode(int _key):key(_key){}
};

class BinomialHeap : public IHeap{
	std::vector<BinomialHeapNode*> roots;
	void trim(){
		while(!roots.empty() && roots.back() == nullptr)roots.pop_back();
	}
	public:
	BinomialHeap(){}
	virtual bool empty(){
		return roots.empty();
	}
	virtual int GetMin();
	virtual void Insert(int key);
	virtual int ExtractMin();
	virtual void MeldWith(IHeap * what);
};
struct LeftistHeapNode{
	int key;
	LeftistHeapNode *l = nullptr, *r = nullptr;
	int rnk = 1;
	LeftistHeapNode(){}
	LeftistHeapNode(int _key):key(_key){}
	void updrnk(){
		int lr = (l == nullptr) ? 0 : l->rnk;
		int rr = (r == nullptr) ? 0 : r->rnk;
		rnk = std::min(lr, rr) + 1;
	}
};

class LeftistHeap : public IHeap{
	LeftistHeapNode * root = nullptr;
	public:
	LeftistHeap(){}
	virtual bool empty(){
		return root == nullptr;
	}
	virtual int GetMin();
	virtual void Insert(int key);
	virtual int ExtractMin();
	virtual void MeldWith(IHeap * what);
};
struct SkewHeapNode{
	int key;
	SkewHeapNode *l = nullptr, *r = nullptr;
	SkewHeapNode(){}
	SkewHeapNode(int _key):key(_key){}
};

class SkewHeap : public IHeap{
	SkewHeapNode * root = nullptr;
	public:
	SkewHeap(){}
	virtual bool empty(){
		return root == nullptr;
	}
	virtual int GetMin();
	virtual void Insert(int key);
	virtual int ExtractMin();
	virtual void MeldWith(IHeap * what);
};
