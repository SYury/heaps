#include "heap.h"


int BinomialHeap::GetMin(){
	int result = INT_MAX;
	for(auto x : roots)if(x != nullptr && x->key < result)result = x->key;
	return result;
}

BinomialHeapNode * _bintreemerge(BinomialHeapNode * l, BinomialHeapNode * r){
	if(l == nullptr)return r;
	if(r == nullptr)return l;
	if(l->key <= r->key){
		l->children.push_back(r);
		return l;
	}
	else return _bintreemerge(r, l);
}

void BinomialHeap::MeldWith(IHeap * ptr){
	BinomialHeap what = *reinterpret_cast<BinomialHeap*>(ptr);
	int len = std::max(roots.size(), what.roots.size());
	BinomialHeapNode * carry = nullptr;
	while(roots.size() < what.roots.size())roots.push_back(nullptr);
	for(int i = 0; i < len; i++){
		BinomialHeapNode * l = (i < (int)roots.size()) ? roots[i] : nullptr;
		BinomialHeapNode * r = (i < (int)what.roots.size()) ? what.roots[i] : nullptr;
		BinomialHeapNode * tmp = _bintreemerge(l, r);
		roots[i] = nullptr;
		if(carry != nullptr){
			if(tmp != nullptr && (int)tmp->children.size() == i){carry = _bintreemerge(carry, tmp);}
			else{roots[i] = carry; carry = tmp;}
		}
		else{
			if(tmp != nullptr && (int)tmp->children.size() == i){roots[i] = tmp;}
			else{carry = tmp;}
		}
	}
	if(carry != nullptr)roots.push_back(carry);
	trim();
}

void BinomialHeap::Insert(int key){
	BinomialHeap tmp;
	tmp.roots.resize(1);
	tmp.roots[0] = new BinomialHeapNode(key);
	this->MeldWith(&tmp);
	trim();
}

int BinomialHeap::ExtractMin(){
	int result = this->GetMin();
	int pos = -1;
	for(int i = 0; i < (int)roots.size(); i++)if(roots[i] != nullptr && roots[i]->key == result)pos = i;
	if(pos == -1)return result;
	BinomialHeap tmp;
	tmp.roots = roots[pos]->children;
	roots[pos] = nullptr;
	this->MeldWith(&tmp);
	trim();
	return result;
}
